package main

import (
	"fmt"
	"html"
	"imt2681-cloud-assignment2/assignment2"
	"log"
	"net/http"
	"os"


	"cloud.google.com/go/firestore"
	"golang.org/x/net/context"

	firebase "firebase.google.com/go"

	"google.golang.org/api/option"
)

var ctx context.Context
var client *firestore.Client


func defaultHandler (w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, this is the main page. To view other pages pleace hit /repocheck/v1/ commits,languages or status%q", html.EscapeString(r.URL.Path))
}


func main () {

	// Firebase init :
	ctx = context.Background()
	// We use a service account, load credentials file that you downloaded from your project's settings menu.
	// Make sure this file is gitignored, it is the access token to the database.
	sa := option.WithCredentialsFile("/home/stian/go/src/imt2681-cloud-assignment2/assignment2/imt2681-assignment2-93320-firebase-adminsdk-6px8z-3fd5534a73.json")
	app, err := firebase.NewApp(ctx, nil, sa)
	if err != nil {
		log.Fatalln(err)
	}

	client, err = app.Firestore(ctx)

	//Sets info for the global DB connection
assignment2.DB.Ctx = ctx
assignment2.DB.Client = client

	if err != nil {
		log.Fatalln(err)
	}
	defer client.Close()

	port := os.Getenv("PORT")

	if port == "" {
		//log.Fatal("$PORT must be set")
		port = "8080"
	}


	http.HandleFunc("/", defaultHandler)
	http.HandleFunc("/repocheck/v1/commits/" , assignment2.CommitsHandler)
	http.HandleFunc("/repocheck/v1/languages/", assignment2.LanguagesHandler)
	http.HandleFunc("/repocheck/v1/status/", assignment2.StatusHandler)
	http.HandleFunc("/repocheck/v1/webhooks/", assignment2.DisplayWebhooks)

	log.Fatal(http.ListenAndServe(":"+port, nil))

}
