package assignment2

import (
	"encoding/json"
	"fmt"
	"html"
	"log"
	"net/http"
	"strconv"
	"time"
)

//Repositories struct with list of structs with the repository info.
type Repositories struct{
	Repos []repInfo `json:"repos"`
	Auth bool `json:"auth"`
}

//repInfo struct with specific info about repository according to assignment specification
type repInfo struct{
	Repository string `json:"path_with_namespace"`
	Commits int `json:"commits"`
}
//Id struct used for gathering id's
type Id struct {
	ID int `json:"id"`
}


//CommitsHandler function takes optional parameter limit and auth token.
func CommitsHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodGet {
		//Checks if there is a authentication token, other wise blank
		auth := ""
		if r.URL.Query()["private_token"] != nil { // see if limit is in the URL
			customToken := r.URL.Query()["private_token"][0]
			auth = customToken
		}
		println(auth + "test auth")

		//checks for custom limit, default is 5
		limit := 5
		if r.URL.Query()["limit"] != nil { // see if limit is in the URL
			customLimit := r.URL.Query()["limit"][0]
			println(customLimit)
			customLimitInt, err := strconv.Atoi(customLimit)
			if err == nil {
				limit = customLimitInt
			}
		}


		//fetches the parameters that should be sendt with the webhook, then checks if there is any webhooks registered for commits.
		parameters := []string{strconv.Itoa(limit)}
		invokeWebhooks("commits", parameters, time.Now())


		//Initialize struct variables
		var Results Repositories
		var repositoryInfo repInfo
		var repositoryID []Id


		commitsSamler := make(map[int]int)

		//Starts at page 1
		pageNumber := 1

		//requests information from git site.
		resp, err := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100&private_token=%s", auth))
		if err != nil {
			log.Fatalln(err)
		}
		//checks if the request done by user is valid or not.
		if resp.StatusCode == 200 {
			numOfPages, err2 := strconv.Atoi(resp.Header.Get("X-Total-Pages")) 		//Fetches the total amount of pages from the previous ge request
			if err2 != nil {
				log.Fatalln(err2)
			}

			//Loops for amount of pages
			for i := 1; i <= numOfPages; i++ {

				//Just for me to see that the program is still alive since the queries takes so long to finish.
				println("tallet er nå ", i)

				//Fetches all repositorie id's for one page
				resp, err := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100&page=%d&private_token=%s", pageNumber, auth))
				if err != nil {
					log.Fatalln(err)
				}

				_ = json.NewDecoder(resp.Body).Decode(&repositoryID)

				//For each repository im getting the X-total value from the header and saves these away.
				for z := 0; z < len(repositoryID); z++ {
					id := repositoryID[z].ID
					resp, err2 := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects/%d/repository/commits?private_token=%s", id, auth))
					if err2 != nil {
						log.Fatalln(err2)
					}

					//Skipped error handling because one repository consistently caused strcon to crash by unknown causes. Works fine without.
					numOfCommits, _ := strconv.Atoi(resp.Header.Get("X-Total"))

					commitsSamler[id] = numOfCommits

				}

				//Next page
				pageNumber ++
			}

			//Loops to find the highest commit found and stores it. Loops amount of times equal to limit specified by user
			for i := 0 ; i < limit ; i++ {
				var largestKey int
				var largestValue int
				for key, value := range commitsSamler {
					if value > largestValue {
						largestValue = value
						largestKey = key
					}
				}
				//Gets the name of said repository
				resp, err := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects/%d?private_token=%s",largestKey, auth))
				if err != nil {
					log.Fatalln(err)
				}

				_ = json.NewDecoder(resp.Body).Decode(&repositoryInfo)

				repositoryInfo.Commits = largestValue

				//Appends the highest found to the list of repositores that is going to be returned to user
				Results.Repos = append(Results.Repos, repositoryInfo)

				//deletes the highest value to avoid duplicate in next loop.
				delete(commitsSamler, largestKey)
			}




			//Very simple check to see if there is an authentication token or not. Does not check validity
			if auth == "" {
				Results.Auth = false
			} else {
				Results.Auth = true
			}

			w.Header().Add("content-type", "application/json")
			_ = json.NewEncoder(w).Encode(Results)

		}else {
			fmt.Fprintf(w, "Malformed request%q", html.EscapeString(r.URL.Path))
		}

	} else {
		fmt.Fprintf(w, "Method not allowed. Only GET requests available%q", html.EscapeString(r.URL.Path))
	}








}

