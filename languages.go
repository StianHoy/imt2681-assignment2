package assignment2

import (
	"encoding/json"
	"fmt"
	"html"
	"log"
	"net/http"
	"strconv"
	"time"
)



//Language struct containing list of languages
type Language struct {
	Name []string `json:"name"`
	Auth bool `json:"auth"`

}

//RepositoryInfo containing the id of each repository
type RepositoryInfo struct {
	Id int `json:"id"`
}

//LanguagesHandler function
func LanguagesHandler(w http.ResponseWriter, r *http.Request) {

	//Checks if this is a get request, rejects in case it is not.
	//Checks if there is a authentication token, other wise blank
	auth := ""
	if r.URL.Query()["private_token"] != nil { // see if limit is in the URL
		customToken := r.URL.Query()["private_token"][0]
		auth = customToken
	}

	//checks for custom limit, default is 5
	limit := 5
	if r.URL.Query()["limit"] != nil { // see if limit is in the URL
		customLimit := r.URL.Query()["limit"][0]
		customLimitInt, err := strconv.Atoi(customLimit)
		if err == nil {
			limit = customLimitInt
		}
	}
	//fetches parameters used then invokes webhook with subscription to languages endpoint
	parameters := []string{strconv.Itoa(limit)}
	invokeWebhooks("languages", parameters, time.Now())

	//Initialize struct variables
	var repositoryId []RepositoryInfo
	var Results Language

	//Starts at page 1
	pageNumber := 1

	languageRanking := make(map[string]int)


	//Fetches body if there is one. Does not handle authentication token from body.
	var userRepId []int
	err := json.NewDecoder(r.Body).Decode(&userRepId)
	if err != nil {
		log.Printf("Failed to decode language Body or is blank", err)
	}

	resp, err := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100&private_token=%s", auth))
	if err != nil {
		log.Fatalln(err)
	}
	//checks if the query went trough, then finds the total number of pages for the request.
	if resp.StatusCode == 200 {
		numOfPages, err2 := strconv.Atoi(resp.Header.Get("X-Total-Pages"))
		if err2 != nil {
			log.Fatalln(err2)
		}

		//Loops trough the found pages
		for i := 1; i <= numOfPages; i++ {

				//Just for me to see that the program is still alive since the queries takes so long to finish.
				println("number is now ", i)
				//Fetches all repository id's
				resp, err := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100&page=%d&private_token=%s", pageNumber, auth))
				if err != nil {
					log.Fatalln(err)
				}

				_ = json.NewDecoder(resp.Body).Decode(&repositoryId)


				//inner loop takes language for each repository found for the first 100 and all following repositories following.
				for z := 0; z < len(repositoryId); z++ {
					id := repositoryId[z].Id

					//checks if the id is in the id's from body specified by user. If found breaks out and this id languages is stored. If there is no body from user
					//this will just stay false.
					inIds := false
					for _, value := range userRepId {
						if id == value {
							inIds = true
							//fmt.Printf("%d var i payload.\n", id)
							break
						}
					}
					//If there is no body from user, or it was in the body from user we save the languages for a repository. otherwise we go past this query.
					if r.Body == http.NoBody || inIds {

						resp, err2 := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects/%d/languages?private_token=%s", id, auth))
						if err2 != nil {
							log.Fatalln(err2)
						}
						//Map values found in /languages to string map. Interface part is not of use and is ignored.
						languages := make(map[string]interface{})
						err2 = json.NewDecoder(resp.Body).Decode(&languages)
						if err2 != nil {
							http.Error(w, "Error decoding GitLab languages response.", http.StatusInternalServerError)
							return
						}

						// Increase language occurrence for every language in repository.
						for key := range languages {
							languageRanking[key]++
						}

						//Add length of the array to i, if it is modulable with 100 i go to next page. Program will probably crash if there is exactly a multiple of 100 projects available.
						//i += len(repositoryId)
						//Next page

					}
				}
			pageNumber ++
		}
			//Loops the amount of times equal to how many languages the user wants
			for i := 0; i < limit; i++ {

				var largestKey string
				var largestValue int
				for key, value := range languageRanking {
					if value > largestValue {
						largestValue = value
						largestKey = key
					}
				}
				//Adding highest value language to results, then deletes it from the map to avoid duplicates.
				//Also checks if the language is not blank so we only display actual languages.
				if largestKey != "" {
					Results.Name = append(Results.Name, largestKey)

					delete(languageRanking, largestKey)
				}
			}
			//Very simple check to see if there is an authentication token or not. Does not check validity
			if auth == "" {
				Results.Auth = false
			} else {
				Results.Auth = true
			}

			w.Header().Add("content-type", "application/json")
			_ = json.NewEncoder(w).Encode(Results)

		} else {
			fmt.Fprintf(w, "Malformed request%q", html.EscapeString(r.URL.Path))
		}

}