module imt2681-cloud-assignment2/assignment2

go 1.13

require (
	cloud.google.com/go/firestore v1.0.0
	firebase.google.com/go v3.10.0+incompatible
	golang.org/x/net v0.0.0-20191028085509-fe3aa8a45271
	google.golang.org/api v0.13.0
)
