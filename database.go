package assignment2

import (
	"bytes"
	"cloud.google.com/go/firestore"
	"context"
	"encoding/json"
	"fmt"
	"google.golang.org/api/iterator"
	"log"
	"net/http"
	"time"
)

//Webhook struct with information needed to store a webhook.
type Webhook struct {
	ID        string    `json:"id"`
	Event     string    `json:"event"`
	URL       string    `json:"url"`
	Timestamp time.Time `json:"timestamp"`
}

//FirestoreDatabase struct for all initializing of databaseinfo.
type FirestoreDatabase struct {
	ProjectID      string
	CollectionName string
	Ctx            context.Context
	Client         *firestore.Client
}

//webhookSend struct with informatino about an event sent to a url stored in webhooks.
type webhookSend struct {
	Event string `json:"event"`
	Params []string `json:"params"`
	Time time.Time `json:"time"`
}
//Only need one connectino to firestore.
var DB FirestoreDatabase





// Function used when invoking webhooks, calling webhook URLs.
//Looks for webhooks in firestore for events equal to event sent when calling the function. Then sends a notification to the URL with the correct event.
func invokeWebhooks(event string, params []string, currentTime time.Time) {
	iter := DB.Client.Collection("webhooks").Documents(DB.Ctx)
	for {
		var webHook Webhook
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Printf("Failed to iterate: %v", err)
		}
		err = doc.DataTo(&webHook)
		if err != nil {
			log.Printf("Failed to parse database response: %v", err)
		}
		if webHook.Event == event {
			requestWebhook := webhookSend{Event: event, Params: params, Time: time.Now()}
			requestBody, err := json.Marshal(requestWebhook)
			if err != nil {
				log.Printf("Failed to marshall webhook call: %v", err)
			}
			_, err = http.Post(webHook.URL, "application/json", bytes.NewBuffer(requestBody))
			if err != nil {
				log.Printf("Not able to send webhook call: %v", err)
			}
		}
	}
}









//Handles all relevant httpmethods, in this case GET, POST and DELETE.
//GET differentiated between having a webhook id in the url or just displaying everyone if there is none.
func DisplayWebhooks (w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case http.MethodGet:
		//Default id is blank, finds eventual new id in the url from user
		webhookId := ""
		webhookId = r.URL.Path[23:]

		//initialize variables
		var iteration *firestore.DocumentIterator
		var webhook Webhook
		var listwebhook []Webhook

		//If not blank, look for spesific id, otherwise fetch all
		if webhookId != "" {
				iteration = DB.Client.Collection("webhooks").Where("ID", "==", webhookId).Documents(DB.Ctx)
		}else {
			iteration = DB.Client.Collection("webhooks").Documents(DB.Ctx)
		}

		//iterate the documents found into a list
		for {
			doc, err := iteration.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				log.Fatalf("Failed to iterate: %v", err)
			}
			if doc != nil {
				err = doc.DataTo(&webhook)
				listwebhook = append(listwebhook, webhook)
			}


			}

			w.Header().Add("content-type", "application/json")
			err := json.NewEncoder(w).Encode(listwebhook)
			if err != nil {
				http.Error(w, "failed to encode webhoks", http.StatusInternalServerError)
				fmt.Println("failed to encode")
				}

				//Method Post decodes json information from user then calls save function to store the new webhook. Checks that the post request has a correct event type.
	case http.MethodPost:
		var posthook Webhook

		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&posthook)
		if err != nil{
			fmt.Println("Failed to decode json from user in post request")
		}

		if posthook.Event == "commits" || posthook.Event == "languages" || posthook.Event == "status" {
			Save(&posthook, w)

		}else {
			http.Error(w, "Invalid type event used. Use 'commits', 'languages' or 'status'", http.StatusBadRequest)
		}

		//Method Delete, takes id from url and calls delete function to delete webhook with this id.
	case http.MethodDelete:
		var deletehook Webhook

		webhookIdentifier := r.URL.Path[23:]
		deletehook.ID = webhookIdentifier

		Delete(&deletehook, w)

	default:
		fmt.Println("")
	}

}

// Saves a webhook in correct format to the firestore database
func  Save(web *Webhook, w http.ResponseWriter)error {
	ref := DB.Client.Collection("webhooks").NewDoc()
	web.ID = ref.ID
	web.Timestamp = time.Now()
	_, err := ref.Set(DB.Ctx, web)
	if err != nil {
		fmt.Println("ERROR saving student to Firestore DB: ", err)
	}

	//Respond to uer with webhook id of created webhook (in postman)
	_, err = fmt.Fprint(w, web.ID)
	if err != nil {
		log.Printf("Failed to write back webhookID: %d", web.ID)
	}
	return nil
}


//Deletes webhook from database with user inputed ID
func Delete(s *Webhook, w http.ResponseWriter) error {
	if s.ID == "0"{
		fmt.Fprint(w, "Not allowed to delete this ID")
	}else {
		_, err := DB.Client.Collection("webhooks").Doc(s.ID).Delete(DB.Ctx)
		if err != nil {
			// Handle any errors in an appropriate way, such as returning them.
			log.Printf("An error has occurred: %s", err)
		} else {
			fmt.Fprintf(w, "Successfully deleted key with id %s", s.ID)
		}

	}
	return nil
}
