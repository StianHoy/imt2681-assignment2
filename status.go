package assignment2

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)


// status struct
type Status struct {
	StatusGit      int    `json:"gitlab"`
	StatusDatabase int    `json:"database"`
	Version        string `json:"version"`
	Uptime         int64  `json:"uptime"`
}


var upTime = time.Now() // this takes a timestamp at the start of the program

func StatusHandler(w http.ResponseWriter, r *http.Request) {
	// I know https://git.gvk.idi.ntnu.no/api/v4/projects is always up, so if I get the data, it is working
	gitResp, err := http.Get("https://git.gvk.idi.ntnu.no/api/v4/projects")
	if err != nil {
		log.Fatalln(err)
	}

	dbStatus := 200 //Assumes it to be ok
	//Doc("0") is reserved for status checks
	_, err = DB.Client.Collection("webhooks").Doc("0").Get(DB.Ctx)
	if err != nil {
		//Can not get to server for unknown reason
		//Gives a Service Unavailable error
		dbStatus = 503
	}
	parameters := []string{"This is from the status site webhook"}
	invokeWebhooks("status", parameters, time.Now())


	//Save informatino to diagData struct
	Statusgit := gitResp.StatusCode

	// Calculates uptime
	newTime := time.Now()
	bigTime := int64(newTime.Sub(upTime) / time.Second)

	//Stores all data into a status struct
	statusData := Status{Statusgit, dbStatus, "v1", bigTime}

	// Prints data to website
	w.Header().Add("content-type", "application/json")
	_ = json.NewEncoder(w).Encode(statusData)

}
