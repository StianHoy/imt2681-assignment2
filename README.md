# imt2681-assignment2

Implemented endpoints are commits, languages, webhooks and status. 
Issues is not implemented according to assignment details specifying the need to only implemeting 2 out of 3 major endpoints. 

Languages takes a post request for payload with the body on format [repositoryId, repositoryId, ...], a.k.a. use int.
Payload does not handle security token. 

Webhooks only sends time of endpoint and limit from user. Authentication token is not sent for obvious security reasons. 



For using the endpoints use the following syntax:   
commits :
Only a / gives you the 5 repositories with the most commits.    
If you want to use a private_token add ?private_token=xxxxx Adding a faulty code will throw an error since it is not recognized by the git api.     
If you want to set a custom limit add a &limit=x after the private token. If you want to use both private token and limit they must be in this order for the program to run properly.   
    
languages:
Only a / gives you the top 5 languages used over all open repositories.     
If you wish to send a payload to select only certain project you can send a POST request with format "[projectId, projectId, ..]" Failing to use numbers will not work. 
If you want to use a private token add ?private_token=xxxxx Adding a faulty code will throw an error. Make sure to not have a trailing / in the url as it will make your token have this / and make it faulty.  
If you want to use a custom limit add a &limit=x after the private token. Again, wanting to use both means they must be in this order.
Example request using both :    
http://localhost:8080/repocheck/v1/languages/?private_token=XXXXX&limit=2   

status:
Only have the landing page.

webhooks:
Only using a / gives you the default page listing all webhooks currently in use.    
You can look up webhooks stored in the database using /webhookID in the url 
You can delete a webhook using /webhookID in the url and using a DELETE request
You can save a new webhook using a POST request and a body following the template:  
    {
      "event": "commits|languages|status" ,
      "url": "your chosen url for recieving notifications on"
    }   
Failing to use either of the 3 events will throw and error.

If the service is not running for some reason please contact me so i can fix this asap :)